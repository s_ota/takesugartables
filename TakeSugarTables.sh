#!/bin/sh
echo
echo ' SugarCRM の DB 名と DB ユーザー・ PW を指定して'
echo ' "./sugar_tables.txt" に記載の特定のテーブル SQL ダンプを取得します。'
echo ' 出力際は "./export_[YYYYMMDD-HHMMSS]" に行います。'
echo

TABLES_TEXT_FILE="./sugar_tables.txt"
if [ ! -e "${TABLES_TEXT_FILE}" ]; then
	echo ' "'"${TABLES_TEXT_FILE}"'" が存在しません。'
	echo '処理を中断します。'
	exit 1
fi

while true;do
	echo "> MySQL SugarCRM DB 名:"
	read DBNAME
	if [ ! -z "${DBNAME}" ]; then
		break;
	else
		echo "SugarCRM の DB 名を指定してください。"
	fi
done
while true;do
	echo "> MySQL ユーザー名"
	read DBUSER
	if [ ! -z "${DBUSER}" ]; then
		break;
	else
		echo "MySQL ユーザー名を指定してください。"
	fi
done
while true;do
	echo "> MySQL ユーザー PW"
	read -sp "Password: " DBPW
	if [ ! -z "${DBPW}" ]; then
		echo
		break;
	else
		echo "MySQLユーザーパスワードを指定してください。"
	fi
done

# echo $DBNAME
# echo $DBUSER
# echo $DBPW
# exit 0

# 疎通確認
mysql -u"${DBUSER}" -p"${DBPW}" -e "USE ${DBNAME};"
if [ $? != 0 ]; then
	echo
	echo "DB接続失敗。"
	echo "処理を中断します。"
	exit 1
fi

# テーブルリスト取得
TABLE_LIST=()
while read line; do
	TABLE_LIST+=("${line}")
done < "${TABLES_TEXT_FILE}"

# テーブルリスト内容確認
echo '----------'
echo
TABLE_COUNT=0
for t in "${TABLE_LIST[@]}"; do
	echo ' - '"${t}"
	let TABLE_COUNT++
done
echo
echo ' (全 '"${TABLE_COUNT}"' 件)'
echo
echo '上記テーブルのレコードを出力します。[Yes/n]'
read DO_EXPORT
case "${DO_EXPORT}" in
	Yes)
		break;
		;;
	*)
		echo 'キャンセルします。'
		exit 0
		;;
esac

# 現在日時の取得
CRNT_DATETIME=`date '+%Y%m%d-%H%M%S'`

# 出力先ディレクトリ作成
EXPORT_DIR='./export_'"${CRNT_DATETIME}"
mkdir "${EXPORT_DIR}"
if [ $? != 0 ]; then
	echo '出力先ディレクトリの作成に失敗。 -> '"${EXPORT_DIR}"
	echo '処理を中断します。'
	exit 1
fi

# SQL 出力
## DBスキーマ出力
mysqldump -u"${DBUSER}" -p"${DBPW}" "${DBNAME}" -d > "${EXPORT_DIR}"/sugar_dump.sql
## 各テーブル出力
for tt in "${TABLE_LIST[@]}"; do
	# mysqldump -u"${DBUSER}" -p"${DBPW}" "${DBNAME}" -t "${tt}" > "${EXPORT_DIR}"/"${CRNT_DATETIME}"_dump_"${tt}".sql
	mysqldump -u"${DBUSER}" -p"${DBPW}" "${DBNAME}" -t "${tt}" > "${EXPORT_DIR}"/sugar_table-"${tt}".sql
	if [ $? != 0 ]; then
		echo '次のテーブルの出力に失敗しました。 -> '"${tt}"
		echo '処理を中断します。'
		exit 1
	fi
done

echo
echo "処理が完了しました。"
exit 0