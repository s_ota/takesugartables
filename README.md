TakeSugarTables
========

SugarCRM の DB スキーマと指定のテーブルダンプを取得します。  
"SugarCRM7開発環境初期化スクリプト" で利用できるSQLファイル名で出力します。  
  
  
## 使用方法

1. ``sugar_tables.txt`` に出力したいテーブル名を一行毎に記述。
2. ``TakeSugarTables.sh`` を実行。
3. ``./export_[YYYYMMDD-HHMMSS]`` に出力されます。
